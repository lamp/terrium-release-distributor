var https = require("https");
var express = require("express");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var serveIndex = require("serve-index");
var fs = require("fs");
var rimraf = require("rimraf");
var request = require("request");
var verifyGithubWebhook = require("verify-github-webhook").default;
var config = require("./config");



var app = express();
app.use(bodyParser.json());
app.use(morgan('combined'));


app.post("/update", function (req, res, next) {
    
    // verify authenticity of webhook request
    if (!verifyGithubWebhook(req.headers["x-hub-signature"], JSON.stringify(req.body), config.github_webhook_token)) {
        return res.status(403).send("you are not github!");
    }

    // ignore non-release events
    if (req.headers["x-github-event"] != "release") {
        return res.status(200).send("ok thanks but i dont need this");
    }
        
    console.log("Received release event from Terrium GitHub:\n", req.body);

    // make sure payload exists
    if (!req.body || !req.body.release) return res.status(400).send("where's my release?!");
    
    res.status(200).send("oh wow a new terrium release! tysm");

    // start distributing release
    var grpath = process.cwd() + "/public";
    console.log("clearing game releases directory");
    rimraf(grpath + "/*", function(er){ // clear the game releases directory
        if (er) console.error(er);
        // download all the assets
        var download_urls = {};
        var i = 0;
        (function iterateAssets(){
            var asset = req.body.release.assets[i++];
            if (asset) {
                // download asset
                let url = asset.url + "?access_token=" + config.github_pat;
                console.log("downloading", url);
                request.get(url, {headers: {
                    Accept: 'application/octet-stream',
                    "User-Agent": "Terrium-Release-Distributor"
                }}) .on('error', error => console.error(error))
                    .pipe(fs.createWriteStream(`${grpath}/${asset.name}`))
                    .on('finish', ()=>{
                        iterateAssets();
                    });
                { // save asset url
                    let name = asset.name.toLowerCase();
                    let plat;
                    if (name.includes("mac") || name.includes("osx"))
                        plat = "mac";
                    else if (name.includes("win"))
                        plat = "windows";
                    else if (name.includes("linux"))
                        plat = "linux";
                    download_urls[plat] = `${config.download_root_url}/${asset.name}`;
                }
            } else { // downloads done
                console.log("finished downloading assets; broadcasting announcements…");
                // send announcement to discord
                request.post({
                    url: config.discord_webhook,
                    json: true,
                    body: {
                        content: `**New TERRIUM Release!**`,
                        embeds: [
                            {
                                color: 44488,
                                title: release.name,
                                description: release.body,
                                fields: [
                                    {
                                        name: "Download Now",
                                        value:
                                            `Select your platform:\n` +
        /* ( ͡° ͜ʖ ͡°) */                      `[» Mac](${download_urls.mac})\n` +
        /* huge indenting lol */            `[» Windows](${download_urls.windows})\n` +
                                            `[» Linux](${download_urls.linux})\n`
                                    }
                                ],
                                //timestamp: release.published_at
                                //TODO parse release.body for image and put as embed image
                                //TODO include author?
                            }
                        ]
                    }
                }, function(err, res, body){
                    if (err) console.error("failed to send announcement to discord", err);
                    else console.log("discord received message with response:", body);
                });
                // update website
                request.post({
                    url: config.website_game_release_webhook.url,
                    headers: {
                        Auth: config.website_game_release_webhook.auth
                    },
                    json: true,
                    body: {
                        version: release.tag_name,
                        download_urls 
                    }
                }, function (err, res, body){
                    if (err) console.error("failed to update website data", err);
                    else console.log("website server responded with:", body);
                });
                //TODO post on forums
            }
        })();
    });
});

app.use("/", express.static("public"), serveIndex("public", {icons : true}));


https.createServer(config.https_options, app).listen(9701);
