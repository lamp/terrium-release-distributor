module.exports = {
	apps: [
		{
			name: "terrium-release-distributor",
			script: "index.js",
			output: "output.log",
			error: "error.log"
		}
	]
};