This thing publicly distributes terrium releases from the private github repository. On a release, github sends a request to this server's webhook (at `/update`), the assets are downloaded to the `public` directory via an authenticated github user `api-terrium-net`, a request is sent to the website's webhook (`/game-release.php`) to update the version number and download links on the website specified by the file `game-release.json`, and an announcement is posted in discord with the release method and download links. Platforms are determined by the case-insensitive inclusion of "mac" or "osx" for the mac build, "win" for the windows build, and "linux" for the linux build, in the file names.

In case of failure, assets can be manually uploaded to the `public` directory (or you can upload your builds to dropbox), and `game-release.json` on the bluehost server can be updated manually.

Currently hosted on ts.terrium.net under user 'terrium', passphrase is `files messrs pools soccer`.
