module.exports = {

    // options for the https server
    https_options: {
        key: require("fs").readFileSync("/etc/letsencrypt/live/ts.terrium.net/privkey.pem", "utf8"),
        cert: require("fs").readFileSync("/etc/letsencrypt/live/ts.terrium.net/fullchain.pem", "utf8"),
    },

    // for verifying GitHub webhook when a new Terrium release is updated
    github_webhook_token:"ebniuwrghwaoeivfhw4th2cpqx389hgq;o3rgjesro-gw45oghwxorsgih",

    // personal access token of api-terrium-net user for downloading game releases from private repo
    github_pat: "c8b570eacf0ddfe504b28b3fdc2040aab1d40042",

    // root url of download location of game releases for generating URLs to distribute
    download_root_url: "https://ts.terrium.net:9701",

    // discord webhook for announcing game release in discord
    discord_webhook: "https://discordapp.com/api/webhooks/489686037535391749/VgLPsXyITBGKT8c0oc5_WYKDxxxlJn6yESElwN0AIIpcwgPE7gVpp-O7aKwozKkLr_ik",

    // for updating the download links on the website
    website_game_release_webhook: {
        url: "https://terrium.net/game-release.php",
        auth: "iuefnvbiwbvipertbgepio2384htx24hgp924j"
    }

}